---
title: "On Behalf of Her Majesty The Queen"
date: 2019-11-18
draft: false
---
A Press Release from the newly formed Government, in the name of Her Majesty the Queen Victoria.

![The Coat of Arms](/diplomacy/Coat_of_arms_of_the_United_Kingdom_(1837-1952).svg)
---

Her Majesty Victoria, by the Grace of God, of the United Kingdom of Great Britain and Ireland Queen, Defender of the Faith, Empress of India is pleased to announce that Sir Nathaniel Ijams, MP of Scotland, has been considered the most capable of forming a majority within Parliament and shall now lead a Government in implementing the will of Her Majesty throughout the home islands, Empire, and world.

---

### Message from the Prime Minister

It is a satisfaction for Britain in these terrible times that no share of the responsibility for these events rests on her. Only in Britain have words of peace and reconciliation rung louder than the speeches of the jingoists and the battle cries of the war hawks. She is not the Jonah in this storm.  The part taken by our country in this conflict, in its origin, and in its conduct, has been as honourable and chivalrous as any part ever taken in any country in any operation.

In a world of chaos, of war, of pain, of starvation, of evil, a country as great as this one, an Empire as great as this one, a leader as great as Her Majesty The Queen is needed to return the focus of the wider globe to those things which matter to us most: the raising of children, the peace of a son at home, the upbringing of a child with both father and mother, the great industrialisation of the workforce, the routine of a day not sullied by the sirens of an air raid. Only in this Empire can the citizen live peacefully, in the full of confidence of her leaders, in the treasure chest of an unbeatable navy and an invincible army. 

I must begin by extending a warm hand to the leaders of any other great nation who wishes to continue in this world without war. Together, peace can be made. Together, the death of the innocent can be avoided. 

But to those who wish only harm and destruction to our island of paradise, I say that this country, this Empire, will stand steadfast. To any who attack: we will attack. To any who betray: we will destroy. To those that sell us out today: you will be the dead fish in our market tomorrow. The United Kingdom will **stand** in the faith of God, under the leadership of Her Majesty The Queen, and under the watchful guide of me, your Prime Minister, and the Parliament and People whom I represent.

God Bless the United Kingdom, and God Save The Queen. 
