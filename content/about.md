+++
title = "About iJams"
date = "2020-09-20"
+++

The personal website of Nate Ijams.

See my [Attributions](/attribution/) page.

---

## Passions

Ethical software, freedom of expression, journalism, history, activism, science,
environmentalism, typography, design, systems, communication, philosophy, books,
physics, biology, zoology, astronomy, metaphysics, logic, ethics, aesthetics,
poetry, theatre, music, rhetoric, psychology, linguistics, economics, politics,
government, and everything else in the world. In other words, I'm an enthusiast
of sorts.

You shouldn't trust this page. It is rarely updated and surely outdated.
