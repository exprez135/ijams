---
title: Loriel d'Myriel
date: 2019-06-29
---

Of Latour, a place far across the sea

*The tale of a tortured soul.*

### Chapter One: Introduction

It has been seven years, he thought suddenly. Seven years since everything had changed, since the world had transformed itself around him in a rush of movement so confusing, so isolating, and so different that perhaps no one could understand, except, maybe, some deep, inner part of himself.

The world around him faded away, the clangs of mugs, the shouts of men, the general hurry of the inn's tavern falling into the void as memories rushed around him, overwhelming his senses, and sending his already silent self into a silence which worried even old Barthos, the innkeeper and long-time friend of the ranger.

Seven years ago he had still resided in the Old Latour. That legend-filled land of beauty and of riches, of winding mountain paths leading to beautiful palaces and beautiful cottages overhanging cliffs with flowing waterfalls. See, the land of Faerûn is immense. It fills voids, it breathes life into millions. But it is also small. Small in the eyes of the more distant lands of Toril. Old Latour was one of these distant lands, until the disaster.

![Image of Latour](/dnd/latour1.JPG)

*Source: The Hidden Archives of the Lords of Waterdeep*

Latour was the land of the eternal moon. The land of whale back. The land of mystical trees. Of ever-changing seas. Of simple paradise itself.

Loriel d'Myriel was the youngest member of the People of Latour. In that function, as was tradition, he was to be sent out into the world to explore and to find himself. Then, he was to return. To leave the fatigues of the outside world to return as a now-wiser man.

Then, the Fire Nation attacked.

Latour was a land of elves and of humans. Of full elves, half-elves, full humans, and every shade between. Loriel was the son of one of these full elves and full humans. He was handsome, he was slender, and always he was drawn to the depths of forest, to the small waterfalls hidden from view. He was in love with Latour, with its beasts and its trees, its insects and its plants. And especially with its women.

![Image of a woman of Latour](/dnd/woman.png)

The time came. He was to leave. So he left. Not with tears, but with determination. Determination to make these next 10 years of exile end as quickly as time can be made quicker.

In a stroke of luck, or of sadness, or of terrible fate, Loriel was the one man of Latour far enough away when the Halflings attacked. In only twenty minutes, their swords and arrows had slaughtered everyone. Latour was, after all, the home of gentle folk, not of warriors. They did not hunt: deer and fowl would arrive, and die peacefully, so that the elves and humans might eat.

Latour was magical, for it was made upon the back of an ancient creature. More ancient than ancient, older than old. Pre-existent, and present since seas became seas and water water. This creature was one of the beacons of magical power which hum through the entire world. Deep magic. Magic beyond the feeble magic of wizards, of rangers, of clerics. Latour rested upon the magic of peace itself.

The halflings attacked, and the peace was broken. It was not destroyed, nothing can destroy it, but it was disturbed, brought too close to danger, it was hurt. The mighty creature which sang its hum under the everlasting moon, sunk. Into the depths of the sea, but beyond sea and into the essence of the peaceful crests of waves. Latour … was gone.

Loriel was not.

---
### Chapter Two: The Sword Coast

For seven years he has been lost. Wandering, learning, hunting, killing. He had become something both rooted in the peace and mercy of Latour, and something beyond the ordinary in Faerûn. In forests by night, on the trails of game by day, visiting small towns upon the way.

The movement of creatures, of the wide forces of nature, brought him many places. Deep into the East and back again to the common roads of the West. Alone almost always, he became a shadow, but a shadow which only concealed the bright self of the past.

**Adventure: To Aelinthaldaar**

While young, Loriel was still well-versed in the histories of his people. Though gone, they were with him always. Each night the half-elf would spend time writing upon the parchment which he meticulously cared for: recording and reciting. And there was one trip he made that was especially different.

In the North of the Sword Coast, lie forests unique. Different in their smell, in their beauty, in the essence of their existence. While traveling through such a forest, he was drawn irresistably to the memory of a certain verse from one of Latour's epic poems which described the sounds of mankind. It read, in part:

All day and night, save winter, every weather,

Above the inn, the smithy, and the shop,

The aspens at the cross-roads talk together

Of rain, until their last leaves fall from the top.

-

Out of the blacksmith’s cavern comes the ringing

Of hammer, shoe, and anvil; out of the inn

The clink, the hum, the roar, the random singing –

The sounds that for these fifty years have been.

-

The whisper of the aspens is not drowned,

And over lightless pane and footless road,

Empty as sky, with every other sound

Not ceasing, calls their ghosts from their abode,

-

A silent smithy, a silent inn, nor fails

In the bare moonlight or the thick-furred gloom,

In tempest or the night of nightingales,

To turn the cross-roads to a ghostly room.

-

And it would be the same were no house near.

Over all sorts of weather, men, and times,

Aspens must shake their leaves and men may hear

But need not listen, more than to my rhymes.

-

Whatever wind blows, while they and I have leaves

We cannot other than an aspen be

That ceaselessly, unreasonably grieves,

Or so men think who like a different tree.


Here, he remembered the tales of Aelinthaldaar. An Elven city abandoned, the last great city to be visited by the People of Latour before their journey across the sea. Now, replaced. To the men and races of the Sword Coast, Aelinthaldaar is the location of the Treasure of Cities, Beauty of the Beyond, City of Splendors: Waterdeep.

This Forest was the crossroads of a long-faded nature, just as Waterdeep was now the crossroads of the North, and even of the entire Coast. Loriel then set out to visit Waterdeep to find some of the lost ruins of Aelinthaldaar, to connect to the history of a people now gone.

The walk to Waterdeep is unlike any other. From the forest, to a road. On the road, few. And then, only some forty miles away, the deep forest begins to make way to cultured fields. The quiet passage, to the sounds of farmer boys playing and carriages bustling. Cows moo, hawks hunt; the sense, the anticipation of the city permeates the air. The land and all its facets *drew* you in, towards the crossroads, towards the city. 

As sunset approached, the city revealed itself.

![Waterdeep by sunset](/dnd/Waterdeep-overview.webp)

A ranger, the reader might think, could not possibly grasp the beauty and sprawl of such a city. Quite the contrary, for even to someone who belongs in the wayside, Waterdeep is like a enveloping excitement. It is not loving. It is not simple. But it is energy. At sunset, this was experienced. The now-weary traveler followed the still-busy road towards the main gate of the city and walked through it as easily as he might walk through the forest before. In a city like Waterdeep, all were welcome.

![Waterdeep Arrival](/dnd/waterdeep-arrival.jpg)

In Waterdeep, the only trouble with finding a room comes from the abundance of choices, each somehow better than the last, each somehow cheaper. Every street offered a bed, a hearth, coffee to drink, and a hearty meal to replenish. It was in one such inn that Loriel found refuge from the slowly cooling evening.

![Waterdeep Inn Street](/dnd/waterdeep-inn.jpg)

After the short but stout innkeeper showed him his room for the night, Loriel idled back to the common tavern room, taking a seat near the large, clear windows which looked over a small grassy slope. A young lady asked him for his order, and he settled into the comfortable chair to stare out into the sky. Occasionally, on nights with few clouds and a cool breeze, the Aurorieali bless the region with their appearance. Lights flashed through the sky above Waterdeep as thousands of young ones looked out of their bedroom windows, content in awe.

![The Aurorieali above Waterdeep](/dnd/waterdeep-night.jpg) 

---
**Personality Traits:**
The common folk love me for my kindness and generosity.
Despite my noble birth, I do not place myself above other folk. We all have the same blood.

**Ideals:**
Respect. Respect is due to me because of my position, but all people regardless of station deserve to be treated with dignity. 
Family. Blood runs thicker than water. 

**Bonds:**
Nothing is more important than the other members of my family.
The companionship of nature.

**Flaws:**
I hide a devastating secret which plagues me daily.
I am not "stable".

Other Traits:
- Hesitant to trust
- Aware
- Reflective
- Wise
- Slow to anger
- Quick to compassion
- Popular with the ladies
- Inquisitive
