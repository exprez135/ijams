+++
title = "D&D Materials"
date = "2019-06-25"
+++

This is an index of the D&D materials hosted on this site for Markos' Grand Compaign.

### Essential
[The Player's Handbook](/dnd/Player-Handbook.pdf)

[Player's Handbook Errata](/dnd/PH-Errata.pdf)

[The Monster Manual](/dnd/Monster-Manual.pdf)

[Monster Manual Errata](/dnd/MM-Errata.pdf)

[Xanathar's Guide to Everything](/dnd/Xanathars-Guide-To-Everything.pdf)

### Our Campaign
[Map](/dnd/Sword-Coast-Map_HighRes.jpg)

[About the Forgotten Realm](https://dnd.wizards.com/dungeons-and-dragons/what-is-dd/forgotten-realms)

[Sword Coast Adventure's Guide](/dnd/sword-coast.pdf)

### Character Sheets
[Sheets](/dnd/sheets) *(note: I'm too lazy to list all these out, if you need one tell me)*

### Special
[Revised Ranger Class](/dnd/UA_RevisedRanger.pdf)

[Companion Sheet](/dnd/companion_sheet.png)
