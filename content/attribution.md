+++
title = "Attributions"
date = "2019-06-04"
+++

See [https://ijams.me/LICENSE](https://ijams.me/LICENSE) for all licensing information.

### Website Design

The website is based on the Hugo theme "shimong". More information can be found [here](https://github.com/emersion/emersion.fr/tree/master/themes/shimong).

The site is managed using [git](https://git-scm.com/), built using [Hugo](gohugo.io), uploaded to [GitLab](https://gitlab.com), and deployed via [Netlify](https://netlify.com).

I manage my email using [Migadu](https://www.migadu.com/en/index.html).


### Images
My favicon is a derivative of [this one](https://svgsilh.com/image/3038409.html), which was published into the Public Domain using a [CC0 License](https://creativecommons.org/publicdomain/zero/1.0/). I made multiple platform versions using the [RealFaviconGenerator site](https://realfavicongenerator.net/).


### Typography

I use and distribute the Charter and Cooper Hewitt typefaces. The [Charter
license](/fonts/charter-license.txt) is unique, and the [Cooper Hewitt
license](/fonts/cooper-hewitt-license.txt) is the SIL Open Font License.

I also use Matthew Butterick's Century Supra typeface. You can [purchase
it](https://mbtype.com) as well.

---
Note: All content on this site is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs (CC BY-NC-ND 4.0) unless otherwise noted. Usually, I might publish under a completely free (as in freedom) license, but I follow the rationale as described by RMS [here](https://stallman.org/articles/online-education.html).

![CC BY-NC-ND](https://ijams.me/license.png/)
