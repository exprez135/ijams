---
title: "Hint 2"
date: 2020-04-04T13:46:40-05:00
draft: false
comments: false
images:
---
Congratulations! You got here!

1 word answers often come together to make wonderful things...

What do you do after you copy something? You ~~~~~ it.

.

What is "rq" shifted forward by one letter?

.

The, this, that, and those all have something in common. What is it, but backwards?

/

When writing, ink might collect into a mass to form a ~~~~?

/

Find a Base64 decoder and decipher "ZDM2MTc1NmIwZDA0YzkzMWNjNTAwY2JjYjdiMDI4NWM2YTkzMTc1Yw==".
