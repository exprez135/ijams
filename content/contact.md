+++
title = "Contact Information"
date = "2019-06-03"
+++

## Email

You can reach me via email: [email] [@] [domain] where email="contact" and domain="ijams.me"

My keys:

<a href="/.well-known/openpgpkey/hu/jh4yxzuazuge3omy73g1xdk693asf3ii" title="PGP key BEED841104580204B38041EF236ACF9085017338" download="ijams.pgp">pgp key</a>

## Other contact methods

### Public Inbox

Send a plain text email to my public inbox: [~exprez135/public-inbox@lists.sr.ht](mailto:~exprez135/public-inbox@lists.sr.ht) ~ see this [mailing list etiquette](https://man.sr.ht/lists.sr.ht/etiquette.md).
