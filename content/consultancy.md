+++ 
title = "Hire me"
alias = "/consulting"
date = "2021-05-13"
draft = true
+++

I help guide your project, team, goal, or idea toward the best possible end. I
offer my services as a consultant and, in many cases, as an implementer. 

First, we'll explore your project or idea. What is it? What are you *really*
trying to do? What does thriving look like? 

Then, I'll work with you to find solutions and opportunities which bring about
that thriving. If that involves technology, I might just be able to help you
implement the changes. Or, I might design/program a solution just for you. If
not, no problem! I have plenty of experience in fields outside technology. 

Let's get started! [Email me at nate@ijams.me](mailto:nate@ijams.me).

### What I do

- Project thinking
- Ideation
- Team building
- Programming (websites, applications) 
- Design (simple, fast, effective)
- Administration, operations, and tech management 
- Teaching tech (computers, programming, software, literacy)
- Tutoring and mentorship. 
- Research, writing, talking.
- *More...* [[Ask Me]](mailto:nate@ijams.me)

#### Contact me if you… 

- want to create a website. 
- want someone to evaluate how you're marketing your product.
- are looking for team chat software, but you don't know what's best... or how
  to work it.
- need someone excellent, but cheap, to manage and maintain a server for you. 
- want to make a difference in your community. How do you move forward?
- have an all-virtual team which is struggling to recreate the effectiveness of
  in-person meetings.
- want to learn computers and need a guide.
- are paying a lot for software. What does free and open source mean?
- need a research assistant.

### Why me 

1. I know how to evaluate and refine ideas and teams to bring about thriving.

2. I can design, implement, operate, and suggest technologies which ease or
   accomplish your goal.  

3. I only work on projects I love. If I know I'll quickly lose
   interest, I will redirect you to the best possible alternative.  

4. I work with *people* to create. You'll have my phone number, my email, and my
   attention.  

5. I take a multidisciplinary, wholistic, and ethical approach to every project. 

### How I do it (*The Garden*, a metaphor)

Our focus is a garden. We want this garden to thrive, grow, and survive. What
does that thriving look like? Perhaps it might look a bit like:

![beautiful green garden photo](https://images.unsplash.com/photo-1547389432-8def40fe9a27?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=664&q=80)

Which factors need to be aligned to allow for that thriving to occur?

For our garden we need the soil, sun, temperature, nutrients, freedom from
pests, and climate to align. Without any of these, thriving will never occur.
Or, perhaps, temporary success will come before disaster.

How do we actually engage with the world to bring about alignment in these
factors?

We might choose a certain place, import the proper soil, create a watering
system, and much more (I must admit, I'm not a very good gardener). 

**Oftentimes**, individuals and teams jump right in with the planting without
considering the true purpose, outcomes, and ideas behind the plants that will
grow. Instead, we need to think about what we're doing and how to expend our
limited resources to achieve thriving.

### Guarantees

If there are better resources that already exist, I'll direct you to them at no
charge. I will never suggest home-grown solutions which are likely to become
costly to maintain (or a solution only I can maintain). I will always prefer
free and open source software that's guaranteed to be available into the future.
If you aren't satisfied with my work, you'll pay nothing. 

### Pricing

Every project is different. I might be evaluating your team and idea to suggest
solutions or implementing projects that take weeks. [Get in
touch](mailto:nate@ijams.me) to find out more about what I can do for you.  

### Who I am

Nathaniel Ijams: Tulsa native, Columbia University (2024), an [enthusiast of
sorts](/about).

### Experience and Testimonials

**CultureBridge Tulsa**: I helped design, found, and implement a philanthropic
project that promotes a greater understanding of the immigrant experience to
inspire the people of Tulsa to empathize with immigrants and to unite against
cultural and political barriers. [[Website]](https://culturebridgetulsa.com)
[[Manual]](https://culturebridge.netlify.app)

**The Taliaferro Times**: the school newspaper and creative writing club of Booker
T. Washington High School in Tulsa, OK.
[[Website]](https://thetaliaferrotimes.org)

**Mailspring Libre**: I help to maintain a fork of the beautiful Mailspring client
which is entirely free of tracking and paid features.
[[Website]](https://mailspringlibre.org)
[[Code]](https://git.sr.ht/~exprez135/mailspring-libre)

**The Iron Square**: I operate and design software for an assorted group of
technologists. I administer and customize gaming servers, bots, automated tasks,
chat networks, and more. [[Website]](https://beans.sbk.sh) [[Example
Software]](https://git.sr.ht/~exprez135/go-hypixel)

I operate, host, or design over two dozen websites.

<!-- TODO NewsDev-->
<!-- TODO testimonials and smaller projects from individual folks -->
<!-- TODO view and mirror after sourcehut.org (home) and
sourcehut.org/consultancy/ (consulting) -->
<!-- TODO create entire business site @ ijams.co -->
