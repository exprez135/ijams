+++
title = "Have a Holli baughy Christmas, it's the best time of the year!"
date = "2021-04-11"
+++

## Current Status


| Person | Form | Literature | Notes |
| ------ | ---- | ---------- | ----- |
|||Claudia Rankine, Citizen: An American Lyric||
|||Homer, Iliad||
|||Sappho, If Not, Winter: Fragments of Sappho||
|||Homer, Odyssey||
|||New Oxford Annotated Bible with Apocrypha: Genesis, Song of Songs, Job||
| Olivia | Collage | Aeschylus, Oresteia||
|||Sophocles, Antigone||
|||Parks, Father Comes Home from the Wars||
| Madeleine & Andrea\*| Video/Pictures | Plato, Symposium||
|||Virgil, Aeneid||
|Andrea\*||Ovid, Metamorphoses||
| Jessica | Poetry/Art | New Oxford Annotated Bible with Apocrypha: Gospels||
|||Saint Augustine, Confessions||
|Andrea\*||Dante Alighieri, Inferno||
|||Michel de Montaigne, Essays||
| Adedayo | Poetry, Music? |Miguel de Cervantes, Don Quixote||
|||Sor Juana Inés de la Cruz, Poems, Protest, and a Dream| I have a poem if no one volunteers - Nate |
|||Jane Austen, Pride and Prejudice||
|||Fyodor Dostoevsky, Crime and Punishment||
|||Joaquim Maria Machado de Assis, Dom Casmurro||
| Karin & Anish | Poetry & Photos | Virginia Woolf, To the Lighthouse||
| Kathryn | Painting | Toni Morrison, Song of Solomon||

\*: Andrea is doing a story which will incorporate elements from multiple works.

## Questions
 
### When to have it in by?

Please send in your notes and contributions as soon as possible! In any case,
please submit by the morning of Sunday, April 18th. 

### Should it be physical? Digital? Both?

We aim to deliver the gift in two formats. 

First, as a physical gift. For those
in NYC, we can collect your contribution from you. For those far away, we can
use the digital version to create a physical version (e.g. printing a photo,
writing out a note, etc.).

Second, we aim for a digital version of the entire project (i.e. a website).
This will include digital versions of everything, scanned versions of art, etc.
Of course, if you prefer your section to not be included on the website just let
me know.

### What about a simple thank you note? I don't have much time!

Totally understandable! You can send in a digital note by filling out the form
below. I recommend you write it elsewhere and paste it here when you are ready.
You can also scan a hand-written note and send it to me if you prefer that! 

<form action="https://formsubmit.co/lithum@ijams.me" method="POST">
     <p>Name</p>
     <input type="text" name="name" placeholder="Name" required>
     <p>Email</p>
     <input type="email" name="email" placeholder="Email" required>
     <p>Note</p>
     <textarea rows="10" cols="30" name="note" placeholder="Note"></textarea>
     <input type="hidden" name="_next" value="https://ijams.me/shared/columbia/lithum/gift.html">
     <input type="hidden" name="_subject" value="LitHum Note Submission">
     <input type="text" name="_honey" style="display:none">
     <input type="hidden" name="_url" value="https://ijams.me/shared/columbia/lithum/gift/">
     <button type="submit">Send</button>
</form> 

<style>table, td, th{border: 1px solid #ddd; border-collapse: collapse;}</style>
<style>table{    display: block;
    overflow-x: auto;}</style>
