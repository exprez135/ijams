---
title: "Nativity of St. John the Forerunner"
date: 2022-06-24
draft: false
comments: false
images:
---

All creation groaned in anticipation of the coming of the Word, and the babe
leapt in Elizabeth’s womb! St. John the Forerunner—you who made straight the way
before the Way—intercede for us and for all the unborn on this day, as
a possible decision in the courts looms. With your nativity, the barrenness of
your mother was destroyed and the silence of your father broken at long last—we
sing with you, Baptizer: let our mouths be filled with Thy praise and let the
waters of true Life flow through our barren souls!
