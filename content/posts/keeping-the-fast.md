---
title: "Keeping the Eastern Fast in a Western World"
date: 2022-03-02
draft: false
comments: false
images:
---

Today, as those around us either mark the beginning of Western Lent on 
this Ash Wednesday, or else celebrate nothing but the commercialized 
leftovers of a Mardi Gras divorced from its full meaning, we can take 
a moment to reflect on what it means to enter our Eastern Lenten Fast 
here, in a very Western world. The calendars of the Eastern and Western 
churches are tantalizingly similar. This year, we celebrate our Pascha 
(Easter) one week after the popular celebration here (April 24th instead 
of the 17th). The Lenten season of the West begins today, while we wait 
until Clean Monday or, more properly, the Sunday service of Forgiveness 
Vespers.

Ash Wednesday is marked by the imposition of ashes, together with that 
solemn declaration: “Remember, man that thou art dust, and unto dust 
thou shalt return” (Gen 3.19). In the Catholic church, today is one of 
two fasting days—together with Good Friday—which is combined with 
abstinence from meat products on Fridays throughout Lent.

By contrast, the Orthodox Lent begins with Forgiveness Vespers. We 
remember first that "If you forgive men their trespasses, your heavenly 
Father will forgive you; but if you do not forgive men their trespasses, 
neither will your heavenly Father forgive you your trespasses" (Matthew 
6:14). And in solemn ceremony, we ask every person in our parish, 
individually and with prostration or bow, to "forgive me, a sinner". 
We respond, with some variation depending on the local practice, "God 
forgives, and I forgive".

For East and West, our first day of Lent is a casting off of earthly 
worries; it is an opportunity to focus again, to constantly turn back 
to, the love and forgiveness of Christ who redeems us, who fills us both 
materially and spiritually; and with this reminder, we turn too to our 
neighbor and forgive him everything, that we might be forgiven as we 
surely fall short. Clean Monday, the start of what appears to most of us 
a stringent period of fasting and abstinence, cannot be separated from 
the first flowering of spring that we anticipate. We restrain ourselves 
in fasting, and turn to prayer, and give to anyone who asks of us, 
because we know that it is only in Christ, through God's good will, 
through Him who does all "for the life of the world"—is the Life of the 
world—that the flowers bloom, and the food grows, and the sun sets and 
rises. Lo, how easy it is to forget! So we dedicate ourselves to 
re-membering: to bringing to mind and to regaining that full experience 
of the reality that already has come into being—the created world, the 
fall of the world, and Christ's victory over death through wilful 
subjection to it.

"The springtime of the Fast has dawned, the flower of repentance has 
begun to open..." we declare in tonight's Vespers service. And so we 
again look to Sunday's Gospel:

"When ye fast, be not, as the hypocrites, of a sad countenance: for they 
disfigure their faces, that they may appear unto men to fast. Verily 
I say unto you, They have their reward. But thou, when thou fastest, 
anoint thine head, and wash thy face, that thou appear not unto men to 
fast, but unto thy Father which is in secret..."

Our treasure is in heaven, not in the earthly successes of fasting. We 
yearn for spiritual growth in the heart, for increased humility, not for 
pride in our great piety. This can be the real struggle of Lent!

But how do we manage with our idiosyncratic calendar and approach, 
surrounded by both Western Christianity and, more obviously, a radically 
different attitude to the months to come from our wider culture? On the 
one hand, we are surrounded at this very moment by the busy and anxious 
life of students in mid-term season (cast off thy earthly cares!). On 
the other, we see a world that continues on and on in the rush of 
appointments, jobs, celebrations, and parties (lay up thy treasures in 
heaven!). On our left hand, we see war, disease, death, starvation, 
inflation, and stock market stresses ("When you take away their spirit, 
they die and return to their dust"). On our right hand, we see the 
coming flowers, the warmer sunshine, the birth of spring ("the earth is 
satisfied with the fruit of Thy work").

Here, like Israel, we are a people set apart. But so too are we to "go 
into all the world and make disciples of all nations, baptizing them in 
the name of the Father, and of the Son and of the Holy Spirit…". We are 
"holy", but "apostolic". We are "one" and "catholic". Lent is a struggle 
of individuals, but also one of a united people. We pray more on our 
own, and we fast on our own, but we also come together with greater 
frequency for services, for liturgy, for agape meals. We continue to go 
to our classes, our jobs. But then we come together to pray and to 
prostrate ourselves before Christ Himself, our King and our God, 
repeating the prayer of St.  Ephraim the Syrian. Where do we find 
ourselves in this Lenten season?  Like the Early Church and all who have 
come after, we are "in" this world but not "of" it. We are part and 
apart. We are fallen and raised up.

So as we reach, at last, this season likewise set apart and yet the 
same, we sing: "Holy, Holy, Holy, art thou our God!" Fill us with thy 
Spirit as we journey as the Body of Christ—One, Holy, Catholic, and 
Apostolic—unto and into and through and by your Death and Resurrection.
