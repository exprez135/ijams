---
title: "City Saints I"
date: 2024-09-01
draft: false
comments: false
images:
---

*Reflections on finding holiness in the city*

As I walk among the streets and avenues of the city, cutting corners only on the
quietest of streets, I notice all the still, captured faces looking at me. They
come on printed out, taped up wallpapers, or else on flashing screens that
startle me sometimes in the night. Most of them have text by their image,
advertising their newest make-up or device, making them out to be improved,
beautified, and perfected by something money can buy.

But I studied medieval history at school, and my favorite memories include my
professor talking about icons of Mary and St. Christopher on practically every
street corner. What were they advertising? Pictures like those depicted the
people perfected, beatified, and impossibly transformed by something deep within
their heart. They had texts too, like ΜΡΘΥ, advertising their names and titles,
the reasons we know them to be holy. They were never printed out, never flashed
with LCD-like intensity, but they were painted carefully, safeguarded under
little wooden houses.

It’s not as if those medieval icons were useless. They might not point me to the
best new deodorant, but didn’t their presence promise something else? I could
have turned to the Theotokos for her intercession. To the giant St. Christopher
I would look for protection as I traveled on my way.

They might have been reminders too. When I was angry, to be calmed. When I was
stressed, to say a prayer. Rather than point to my financial struggle, the
things I wished I had, they would remind me of the alms I could be giving to the
beggars on the street.

And it was then, thinking about taped up posters and wished-for icons on city
corners, that I thought about the other faces I was seeing. Walking by me, or
lying on these corners, were living, flashing, or quite-still faces. Starting to
look at them, I saw many things.

Some were no better than the ads and posters. They were still, stuck in some
mire of depression, or else the whole person seemed a living advertisement,
selling themselves off to anyone who’d look. Flashing clothes revealed hefty
prices.

But some I saw, the ones I noticed also seeing. Their eyes mimicked the
spiritual eyes, attentive to one thing only. And in them I saw who all should
be, living advertisements for God’s Kingdom coming.

---

Published in the *Urban Pilgrim*, September 2024 edition.
