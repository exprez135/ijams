---
title: "The Taliaferro Times: Technical Reading List"
date: 2019-06-10
draft: true
---
*Note: this page will be taken down within a couple days so make a copy.*

So you've signed up to learn about how the newspaper runs its website? Hehehe.

**Topics to be covered**:

Website itself:

1. Git & GitLab

2. Hugo

3. Terminal, Bash, and SSH

4. Netlify

5. Markdown

6. HTML

Management, Hosting, and More:

1. TMDHosting

2. CPanel

3. ProjectSend

---
### Git & GitLab

Git is a "version control system". In other words, it helps to manage the history of a site and collaboration between developers as they continue to make changes.

Guide to Git: http://rogerdudler.github.io/git-guide/

For help while using: https://gitexplorer.com/

**You** need to make an account at https://gitlab.com. This is what I use to host the content which is distributed at https://hugo.thetaliaferrotimes.org.

### Hugo

Hugo is the heart of the website (thus the namesake of the page). In essence, Hugo is a program which takes content written in *Markdown* (see below) and converts it into a ready-to-go static site. A static site means the website at hugo.thetaliaferrotimes.org does not involve server work at all. The page content is simply sent to the user's browser and displayed. No fancy back-end work.

The Hugo site: https://gohugo.io

I would recommend playing around with the different demos on that site to get an understanding of how it works.

### Terminal, Bash, and SSH

Wait until later.

### Netlify

Netlify is the free service which basically does the following:
1. Detects when we make a change to the GitLab repository via a push
2. Then takes the updated repository and runs the hugo command on it, which generates a "Public" folder.
3. This generated site is then deployed to https://taliaferro.tk.

Now, in a simple world, we could directly deploy to thetaliaferrotimes.org and that would be it. However, since the website also facilitates other things including the original WordPress site, the new system for submissions (ProjectSend), and more, we cannot do that. So instead, I have a script which then pulls the new repository to the website hosting (TMDHosting) and deploys it that way.

### Markdown

Markdown is a simple "language" that allows you using some key symbols and formatting, to create beautiful content. In fact, this page was written in Markdown. Then, since I'm putting it on my website, I'm also using Hugo (see above) to generate HTML, CSS, etc. for it.

A random guide I found: https://guides.github.com/features/mastering-markdown/

### HTML

HTML is the basis of everything on the web. You don't necessarily need to specifically learn it but you will be familiar by exposure to it eventually. Again, the markdown content of the posts I add to the site repository is converted to HTML via Hugo. So, in the end, HTML is what is being used to show articles to the user.

---

### TMDHosting

TMDHosting is the company which we used to pay for three years of the website domain name (thetaliaferrotimes.org and its subdomains) as well as hosting. So that is where the content which is actually shown is hosted, etc. One day you will get the login info for this.

### CPanel

The site management "panel" which puts together everything about the hosting.

### ProjectSend

ProjectSend is the new system which we will be using for article submissions and then distributing articles to the proper editors. It will be at https://editors.thetaliaferrotimes.org. Once I'm done configuring stuff I will make sure to teach you everything about it.

---

---

---

Other random things:
- I recommend that you download "Atom", which is a text editor which is very useful. Also, it has built in Markdown rendering so you can simultaneously write in markdown and see the resulting, formatted, stuff.
- Are you running a Windows computer?
- We will find a time to meet.
