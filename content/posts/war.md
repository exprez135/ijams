---
title: "On War"
date: 2021-11-20
draft: false
comments: false
images:
---

*A letter intended for reading aloud to the debating recipients*

Beloved brothers and sisters,

How good it is in this month, the same month as Armistice Day, the same month as
our celebration of St. Martin the Merciful of Tours, to address you—by proxy—in
this letter. Still, there is no better month for us to debate and consider if
*war is good for society*. I tell you—strongly and firmly—that it is not good.

Why is it that we here begin our gatherings with the singing of a song? Why is
that we end with song? And why is it that we return to our acknowledged
*omphalós* to break bread together in fellowship? Perhaps it is as the Psalmist
sings: «Behold, how good and pleasant it is when brothers dwell in unity.»

It is this unity that defines the blessing of our fellowship—that defines
love—that shows, yes, even who God is.

So, what shows symptoms of the disunity of our world? Perhaps it is the first
murder, the scattering of people, the confusion of our many languages, the
hatred and self-righteousness in our hearts. Certainly, it is not love.
Certainly, it is not life. 

Love and life, then, should be the center of our conversation today. Is war part
of love and life? Necessary for love and life? Good by association with a desire
for love and life?

Well, love is always a risky business. Love—knowing each other, being together,
dwelling in unity—is a terribly vulnerable endeavour. It is much easier, really,
to talk about the rules. When is it alright for me to kill? When is
self-defensive justified? When is war just? When should I serve? When should I
not? Here, in this territory, we can look for the seven conditions, or the four
maxims, or the twelve rules for life. But I'm sorry to say: none of this is
sufficient. None of this will save you. None of this will solve the existential
problem which we all face.

War is the bringing about of death and corruption by us—we human beings. In its
wake come orphans, widows, graves, rotting bodies left alone, fire, chaos,
messes, starvation, disease, explosions, suffering, crying, lamentation, and
every evil thing. Also in its wake come PTSD, guilt, self-crippling questions, a
terribly difficult infliction in the heart of all involved. War comes with every
evil, every sin, every flaw, every problem—in great number, by human intention,
and willingly. War is a curse on the human race—for it centers around the
willful infliction of that chief and primary symptom of deficiency: death.

The glorification of war in any form is a glorification of death. It is a spear
in the side of the Life-giver.

No killing of another human being is good, no matter how "justified". Because, I
hate to tell you, perfection does not come about just by absence of wrongdoing.
Even the most justified of killings, in war or not, harms. For it is impossible
for us to harm our neighbor without harming ourselves. And it is impossible for
us to harm ourselves without harming our neighbor. We are here to dwell in
unity—to love—and death is absence of this. 

So, when the most righteous of warriors—also the most unwilling of
warriors—kills, he laments. He cries aloud in pain for he knows that he has done
the unthinkable. The unthinkable even for the innocent child he defends. Killing
in war, or anywhere, wounds the soldier. And, obviously, it wounds the victim.

For we are **all** victims of death, my friends. Even the most evil among human
kind are victims here. Killing *always* distances us from life. Killing *always*
harms and kills one's own. Killing is the anti-being, the anti-I AM, the
anti-love. All of us are to be one, to be in unity, to be together in love. One
might not be guilty of transgression in just war or just violence: but he does
now have a vast and dreadful barrier between himself and salvation. No
punishment is necessary, but *healing* is. 

So tell me not of lesser evils being good, or necessary killing being good, or
even of war being justified. For the most just of wars is still something to be
lamented. We all ought to think of war, and to cry aloud: Eloi, Eloi, my God, my
God, forgive us, for we know not what we do.

Signed,

In peace,

Nathaniel Ijams.

