+++
title = "Changes"
date = "2020-08-31"
draft = true
+++

It's the start of new month, as I write this in the last few minutes of the day. There have been many changes, to myself and to the world. As we set in for the future, I want to make better changes:

## Routine

Wake up. Do not rise from bed until you have decided upon your task for the day (perhaps this will be impossible during college). Exercise. Shower and dress. Make smoothie. Grind coffee, brew coffee, drink coffee. Limit yourself to as few cups of coffee as you can. Begin the task. Finish task by lunch. Spend time cooking. Walk. Explore, read, do research. Dinner. Relax. Before bed, begin to decide what tomorrow's task will be.

Inspiration: Devine Lu Linvega ~ https://wiki.xxiivv.com/site/routine.html

## Nutrition

I have adopted a whole foods, plant based diet. Veganism at last! 

## Devices

Use less power. Don't waste. Offline before online. Simplicity. Hardware and software go together.

## Technology

Use less power. Don't waste. Offline before online. Simplicity. Software and hardware go together.

## This Site

While Hugo is great, is it too much? Is the simplicity of markdown enough to justify a large Go program, even if it does generate a static site quickly? I am going to consider switching to, or supplementing this site with, a new wiki-based format like the one provided by [Oscean](https://wiki.xxiivv.com/site/oscean.html).

---

Update:

See the new wiki at https://wiki.ijams.me.
