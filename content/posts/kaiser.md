---
title: "My Response to 'The Kaiser System'"
date: 2020-05-09T22:12:16-05:00
draft: true
comments: false
images:
---

Note: this post is a draft and is not meant as an unequivocal statement of my full ideas or opinions on the matter. Instead, it is an initial & personal response. No guarantee is made as to the finality of my ideas, and I have surely ignored some important information.

Nevertheless, I welcome feedback, criticism, and discussion via my email ([contact@ijams.me](mailto:contact@ijams.me)) or via plain text email to my public inbox ([~exprez135/public-inbox@lists.sr.ht](mailto:~exprez135/public-inbox@lists.sr.ht)) ~ see this [mailing list etiquette](https://man.sr.ht/lists.sr.ht/etiquette.md).

---

Written in response to "The Kaiser System" by Michael Mason: [Link](https://medium.com/@michaelpaulmason/the-kaiser-system-e8c14bca395).

No one can deny that Kaiser[^1] is doing a great deal of good. His money and his intelligence and his steady array of advisors allow the work he does to bring great projects to Tulsa, Oklahoma. I have no gripe with his philanthropy or with Kaiser himself. Instead, I am only wary when it comes to the ways in which his organisations and the government intertwine. In the end, we must look and investigate the extent to which Tulsa is governed by private & unelected interests. Yes, they do good now. But what about tomorrow? What about the next Kaiser? What dangers does this framework create when the next philanthropist is considered a philanthropist only by some, and a villain by others? And are current policies being made in a way which ignores the interests of certain minorities, without being given the same attention and accountability that they would be given as a truly public project?

### My Principles:

- Transparency: both in government and in the foundations and organisations which integrate themselves into government. Every step should be taken to make decisions in the light of day. Records should be kept. Recusals should be made. The same amount of interest should be shown by the public and by journalists in the affairs of these NGOs as in the affairs of the government; and, ideally, more interest should be shown in both than is currently present.

- Public control of public-private partnerships: we have the capacity to demand more accountability from the private organisations which are given projects and favourable treatment by the government. Demand more.

Many of the people of Tulsa look upon the work being done and rejoice. Improvements in parks, education, arts, and more are all being carried out. These goals are venerable and respected. But there are some problems:

- The Silent Disadvantaged: who are we forgetting? Who is overlooked as decisions are made behind closed doors? What impact do a set of decision-makers that look and act the same way have on the people of Tulsa that don't act or look the same way?

- The Bad Emperor problem: does the system, as it is set up today, properly create safeguards for the day on which the current actors are exchanged for those that are trying to harm your interests? For the day when the new Kaiser is not the friendly man we know but one who hungers for more power or more success?

### Guiding Questions:

- First, has the government shown that the private option/connection/system would be effective at solving the problem?

- Second, if the government shows efficacy, we should ask: would the private option/connection/system do too much harm to our open and democratic system?

- Third, if the government shows efficacy, and the harm to our system is not excessive, we should ask: are there sufficient safeguards around the system which prevents abuse, discrimination, and secrecy?

In many ways, I think the first two questions can be sufficiently answered. But the third is more ambiguous. Mason's exploration of Tulsa's current state seems to show a Tulsa in which there are few safeguards and many secrets. It is time to ask questions, read the small text, and get involved.

#### Limitations:

The questions are obviously limited. Who defines what "the problem" is in the first place? Nevertheless, they might provide some guide in determining whether the intertwining of public and private has gone too far.

### Conclusion

In the end, I believe that it is possible for philanthropy to do good for the people of a system while simultaneously, and unintentionally, harming the open structures of that system. We should strive to ensure that decisions aimed to help the public are still approved by the public. The justification cannot be "it is for your own good". Yes, it might be. But the decision should still be ours. Otherwise, we cede our freedom and our autonomy unnecessarily.

I look to the vision of a "a robust, participatory, and pluralist civil society" as set out by Neil Netanel in his essay "Copyright and a Democratic Civil Society"[^2]. It is a world full of "unions, churches, political and social movements, civic and neighborhood associations, schools of thought, and educational institutions." Philanthropy belongs there. It can even be **necessary** for democratic governance. But when it comes to the point at which many decisions are made in conversations I cannot take part in, I worry.

**P.S.** I still love you Kaiser.

[^1]: I use "Kaiser" simultaneously to reference Mr. Kaiser himself, the organisations and subordinates that are involved with him, and as a type for the entire system and potential future systems.
[^2]: Though he discusses this ideal world in the context of copyright and copyright law, it can easily be applied to other areas of policy and decision-making: https://digitalcommons.law.yale.edu/cgi/viewcontent.cgi?article=7715&context=ylj
