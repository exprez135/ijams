---
title: "Out of an abundance of caution…"
date: 2022-04-14
draft: true 
comments: false
images:
---

*An email sent to Columbia administrators out of concern for continuing
restrictions in April 2022.*

Dear administrators and staff of Columbia University,

I write to you today, seriously concerned about ongoing decisions by many of
you—either directly or indirectly—in response to the ongoing endemic of
COVID-19. In sum, I wish to express my severe and sincere displeasure at ongoing
restrictions and requirements, including masking in non-medical settings, the
booster mandate, vaccination-checking, and visitor policies. For how long will
Columbia continue to dictate the course of our campus life with words like “out
of an abundance of caution”? Out of an abundance of caution, nothing. It is time
to stop leashing and start letting.

Masks should not be required in classrooms. Not only is the current policy
seemingly symbolic, as it fails to account for every other hour of life outside
the classroom, but it hurts our classroom setting. Seeing faces, watching the
minute changes of expression as wonder or shock at texts and discussions takes
over, creating genuine community with acquaintances-still-becoming-friends
… this is the classroom we need! To change the policy backwards because of
a variant causing a spike in a 99.9% vaccinated campus, with extremely few cases
of severe illness, is a regression that is hard to understand. Where is the
justification? “Out of an abundance of caution” is not, by any means, enough.
Are we wrong in this? Is there some scientifically sound reason? Do you have
data we cannot see? Make public, then, all your Task Force meetings, your e-mail
correspondence, and your documents. We are not a government, you reply? We have
no Open Records Act, you quip? And? Do we, as a University, unnecessarily
restrict free speech because we are private? Have we committed ourselves to
mediocrity and fear for nothing? Out of a good heart unhinged from good reason?

Lift the booster mandate. For those who have not received a booster, the choice
should be free. Every requirement should consider its subsequent burden on the
freedom of students, especially healthy students for whom there is very little
risk after zero, one, or two shots.

Our campus should be open to all, not restricted to the card-carrying
vaccinated. Access to campus spaces and visitor approval need not rely on any
individual’s vaccination status. This has to happen at some point, so do it now.
Why now, you ask? Why not?

For those of you on the COVID Task Force: I ask you to immediately recommend
changes to Columbia’s policies in masking and vaccination. For those of you with
the power to actually change the policies: change them. For those who sit
between administration and the Trustees: forward to them my concerns; I assure
you they are very widespread. For those with no power: inquire, ask, and
petition; use your voice to work for a return to normalcy—a new normalcy full of
thanksgiving for the solidarity, sacrifices, and solutions that have been made
possible through love of heart and exertion of mind. A new kind of normalcy with
ever-closer awareness of our mortality. For as C. S. Lewis said in 1939:

«Life has never been normal. Even those periods which we think most tranquil,
like the nineteenth century, turn out, on closer inspection, to be full of
crises, alarms, difficulties, emergencies. Plausible reasons have never been
lacking for putting off all merely cultural activities until some imminent
danger has been averted or some crying injustice put right. But humanity long
ago chose to neglect those plausible reasons. They wanted knowledge and beauty
now, and would not wait for the suitable moment that never comes. […] if we
looked for something that would turn the present world from a place of
pilgrimage into a permanent city satisfying the soul . . . we are disillusioned,
and not a moment too soon»

Give us, I beg you, a place to freely seek that knowledge and beauty. Now!

With all sincerity and hope,

Nathaniel Ijams, CC’24
