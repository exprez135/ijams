---
title: "Rules & More"
date: 2020-04-16T14:10:58-05:00
lastmod: 2025-02-21T17:40-04:00
toc: true
draft: false
images:
tags:
  - rules
---

## Email

### Plaintext

Write your emails in plaintext. Do not use HTML or RTF.

See https://useplaintext.email.

### Wrapping

Wrap your emails to 72 columns/characters

See https://useplaintext.email/#etiquette.

### Bottom-Posting

Use bottom-posting or inline replying, unless you have a reason to use top-posting.

See https://useplaintext.email/#etiquette and http://www.idallen.com/topposting.html.

### Open Standards

When sending attachments, use open standards. That means no Microsoft Word documents. Instead, use PDFs for static documents or ODT files for documents that need to be edited (hint: you can open ODT files in most word processors!).


## “Artificial Intelligence”

In any communication to another person, acknowledge the use of any large
language model-based tools (or similar “machine learning” models) in writing or
ideation, unless used solely for basic spelling and grammar checking. This is
a matter of intellectual honesty, of genuine representation to the other, and of
respect for your neighbor.

Generative “AI” provides only predictions. Those predictions are not neccesarily
facts, very often are nonsense, or just lack some level of sense, and need to
be verified and tested. Generative “AI” cannot be held responsible for anything;
the person who uses it is entirely and fully responsible and accountable for
what it produces and how that product is further used.

If you are designing software that uses ML models, any interaction with a person
should include an explicit disclaimer of their use. Any such interactions meant
to replicate ordinary human communication should allow for an alternative that
does not use ML.

Example #1: A chat box that by default includes communication to a machine
(without the use of machine learning): «Hello! I am a chat bot, not a person.
Type PERSON to be routed to a real human. What can I help you with?»

Example #2: A chat box that by default includes communication to a machine that
uses machine learning models: «Hello! I am a chat bot, not a person. To
communicate, I use a modified machine learning model from OpenAI. Read more
about how I work: https://example.com. Type PERSON to be routed to a real human
being. What can I help you with?» 

## Copyright

https://ijams.me/posts/copyright

## Hacker

*From the Jargon File, version 4.4.8.*

> hacker: n.
>
>    [originally, someone who makes furniture with an axe]
>
>    1. A person who enjoys exploring the details of programmable systems and how to stretch their capabilities, as opposed to most users, who prefer to learn only the minimum necessary. RFC1392, the Internet Users' Glossary, usefully amplifies this as: A person who delights in having an intimate understanding of the internal workings of a system, computers and computer networks in particular.
>
>    2. One who programs enthusiastically (even obsessively) or who enjoys programming rather than just theorizing about programming.
>
>    3. A person capable of appreciating hack value.
>
>    4. A person who is good at programming quickly.
>
>    5. An expert at a particular program, or one who frequently does work using it or on it; as in ‘a Unix hacker’. (Definitions 1 through 5 are correlated, and people who fit them congregate.)
>
>    6. An expert or enthusiast of any kind. One might be an astronomy hacker, for example.
>
>    7. One who enjoys the intellectual challenge of creatively overcoming or circumventing limitations.
>
>    8. [deprecated] A malicious meddler who tries to discover sensitive information by poking around. Hence password hacker, network hacker. The correct term for this sense is cracker.
>
>    The term ‘hacker’ also tends to connote membership in the global community defined by the net (see the network. For discussion of some of the basics of this culture, see the [How To Become A Hacker FAQ](http://www.catb.org/~esr/faqs/hacker-howto.html). It also implies that the person described is seen to subscribe to some version of the hacker ethic (see hacker ethic).
>
>    It is better to be described as a hacker by others than to describe oneself that way. Hackers consider themselves something of an elite (a meritocracy based on ability), though one to which new members are gladly welcome. There is thus a certain ego satisfaction to be had in identifying yourself as a hacker (but if you claim to be one and are not, you'll quickly be labeled bogus). See also geek, wannabee.
>
>    This term seems to have been first adopted as a badge in the 1960s by the hacker culture surrounding TMRC and the MIT AI Lab. We have a report that it was used in a sense close to this entry's by teenage radio hams and electronics tinkerers in the mid-1950s.

