---
title: The Manual
author: Nate Ijams
date: 2020-04-03
draft: false
---
[Online Version](https://ijams.me/ypi/manual/)

[PDF Version](https://ijams.me/ypi/manual.pdf)

## What is it?
The CultureBridge Manual is the master instruction document for the CultureBridge program. It contains the mission, purpose, instructions, ideas, documents, and questions which make up the project.

## Why is it important?
This **is** the project[^1]. CultureBridge, of course, lives within us and those who participate with us. But that isn't enough. CultureBridge has to take on its own form. The Manual is that form. With it, anyone should be able to understand who we are, what we are, how we are, why we are. Then, they should be able to put on their own simulation. That might be our future partner or someone in a completely different city (e.g. CultureBridge Chicago, CultureBridge NYC, or CultureBridge Muskogee).

## How is it split up?
It is split into Modules and Supplements. This is a list of the contents[^2]:

### Module Section
The module section is like a guide. Each "module" is just a different part of the Manual. Think of this as a quick-start guide, a summary, or an instruction booklet. Overall, this section will describe **what** CultureBridge is, **how** to put on the simulation, and **why** we did everything the way we did. For example, the "Introduction" module will describe how to give the introduction presentation, why we made the Intro the way we did, the importance of the Intro, and more.

- [ ]  Cover Page
- [ ]  Contents
- [x]  **Executive Summary**
- [x]  **PR Packet**
- [ ]  Overview of the Process
- [ ]  Set-up, Supplies, & Layout
- [ ]  Introduction
- [ ]  Simulation (plus: help/cheat-sheet/guide for each station)
- [ ]  *Debrief* (plus: Debrief Cheat-Sheet for debriefers)
- [ ]  Survey Instructions, Data Practices, and Analysis
- [ ]  Public Relations Instructions

### Supplement Section
The supplements are less of a guide or explanation and more of a "this is a thing we made, look at how cool it is". These are just documents, presentations, images, etc. that are all a part of CultureBridge. For example, the Simulation Module (above) will describe how to do the overall simulation part of the program, and will link to/mention/reference the "Stories" supplement and "Activities" supplement. In other words, the Simulation Module describes **how** to use the stories and activities to actually do what we do in a simulation.

- [ ]  *Intro presentation & script*
- [ ]  *Stories*
- [ ]  *Activities Supplement*
- [ ]  Layout maps
- [ ]  *Surveys*
- [ ]  Emails
- [ ]  *Action Packet*
- [ ]  *Website*
- [x]  **Logos**
- [ ]  *Promo video*
- [ ]  Simulation Kit (with physical objects)

## Where is it?
Right now, it lives in countless documents and folders on Basecamp, Google Drives, and personal computers. Everything, ideally, should be added to the Cohort 11 Basecamp under Docs & Files in its respective folder (Modules or Supplements)[^3].

### What comes next?
1. Each committee and person continues to create, refine, and edit their Module and Supplements.
2. The Modules will eventually be able to be put together in one Manual document. If someone asked for our project, they would receive two things: a document with all the Modules and a folder with all of the Supplements inside.
3. Then, to create maximum portability and usefulness, everything will also be put online in the form of a Wiki (tentatively). This will allow us to create a cross-referenced and contained site which uses well-maintained and well-loved software (i.e. MediaWiki). It also lets us easily implement plenty of extra information we might not put in the document form described in #2. For example, we can put a history of different partners, bios for each of us, lists of resources, etc. The current demo of our wiki is found at https://culturebridge.miraheze.org. Go there and take a look at the Main Page and blue links for examples of what pages might look like (e.g. the Youth Philanthropy Initiative link and the New Tulsans Initiative link).

## I have questions
As always, please contact Nate via Basecamp (Campfire or Ping), e-mail (...), or phone (...) if you have any questions or concerns.

[^1]: As I state after this, I know that it isn't **the** project. But nevertheless, it will be to whoever looks at this in ten years.
[^2]: As far as we know right now. Content will probably be added, subtracted, and moved around before we are all done with CultureBridge Tulsa.
[^3]: Please do this if you haven't already.
